import React, { Component } from 'react';
import './SwitchApp.css';

class SwitchApp extends Component {

  showToDoList = () => {
    this.props.history.push('/todolist');
  };

  showWatchFilms = () => {
    this.props.history.push('/watchfilms');
  };

  render() {
    return (
      <div className="SwitchApp">
        <div className='toDoListForm'>
          <p>Show To Do List app</p>
          <button onClick={this.showToDoList}>Look</button>
        </div>
        <div className='watchFilmsForm'>
          <p>Show Watch Films app</p>
          <button onClick={this.showWatchFilms}>Look</button>
        </div>
      </div>
    );
  }
}

export default SwitchApp;

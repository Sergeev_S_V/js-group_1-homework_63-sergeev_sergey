import React, {Component, Fragment} from 'react';
import AddForm from "../../Components/AddFilmsForm/AddForm";
import axios from 'axios';
import NewFilm from "../../Components/NewFilm/NewFilm";
import './WatchFilms.css';
import Spinner from "../../Components/UI/Spinner/Spinner";

class WatchFilms extends Component {

  state = {
    newFilms: [],
    filmName: '',
    loading: false,
  };

  currentName = (event) => {
    this.setState({filmName: event.target.value});
  };

  getFilmsFromAPI = () => {
    this.setState({loading: true}, () => {
      axios.get('watchFilms.json')
        .then(response => {
          if (response) {
            this.setState({newFilms: response.data, loading: false})
          }
        })
    });
  };

  addNewFilm = () => {
    this.setState({loading: true}, () => {
      axios.post('watchFilms.json', {name: this.state.filmName})
        .then(response => response
          ? this.getFilmsFromAPI()
          : null)
    });
  };

  removeFilm = (key) => {
    this.setState({loading: true}, () => {
      axios.delete(`watchFilms/${key}.json`)
        .then(response => response
          ? this.getFilmsFromAPI()
          : null)
    });
  };

  changeName = (event, key) => {
    let newFilms = {...this.state.newFilms};
    let newFilm = {...this.state.newFilms[key]};
    newFilm.name = event.target.value;
    newFilms[key] = newFilm;

    this.setState({newFilms});
  };

  blurHandler = (event, key) => {
    this.setState({loading: true}, () => {
      axios.patch(`watchFilms/${key}.json`, {name: event.target.value})
        .then(() => this.getFilmsFromAPI())
    });
  };

  componentDidMount() {
    this.getFilmsFromAPI();
  }

  goBack = () => {
    this.props.history.goBack();
  };


  render() {
    let watchFilms = (
      <div className="WatchFilms">
        <AddForm name={this.currentName}
                 click={this.addNewFilm}
                 back={this.goBack}/>
        {this.state.newFilms
          ? Object.keys(this.state.newFilms).map(key => {
            return <NewFilm key={key}
                            name={this.state.newFilms[key].name}
                            change={(event) => this.changeName(event, key)}
                            remove={() => this.removeFilm(key)}
                            blur={(event) => this.blurHandler(event, key)}/>
          })
          : <p>Add new film</p>}
      </div>
    );

    this.state.loading ? watchFilms = <Spinner/> : null;

    return (
      <Fragment>
        {watchFilms}
      </Fragment>
    );
  }
}

export default WatchFilms;

import React, { Component } from 'react';
import './ToDoList.css';
import AddTaskForm from "../../Components/AddTaskForm/AddTaskForm";
import axios from 'axios';
import Task from "../../Components/Task/Task";
import Spinner from "../../Components/UI/Spinner/Spinner";

class ToDoList extends Component {
  state = {
    tasks: [],
    currentText: '',
    loading: false
  };

  currentText = (event) => {
    let currentText = event.target.value;

    this.setState({currentText});
  };

  addNewTask = () => {
    if (this.state.currentText.length > 0) {
      this.setState({loading: true}, () => {
        axios.post('/toDoList.json',
          {text: this.state.currentText})
          .then(() => this.getTasksFromAPI());
      });
    }
  };

  removeTask = (key) => {
    this.setState({loading: true}, () => {
      axios.delete(`toDoList/${key}.json`)
        .then(() => this.getTasksFromAPI())
    });
  };

  goBack = () => {
    this.props.history.goBack();
  };

  getTasksFromAPI = () => {
    this.setState({loading: true}, () => {
      axios.get('/toDoList.json')
        .then(tasks => {
          if (tasks) {
            this.setState({tasks: tasks.data, loading: false});
          }
        })
    });
  };

  componentDidMount() {
    this.getTasksFromAPI();
  };

  render() {
    let toDoList = (
      <div className='Container'>
        <AddTaskForm
          add={this.addNewTask}
          text={this.currentText}
        />
        <div className='Tasks'>
          {this.state.tasks
            ? Object.keys(this.state.tasks).map(key => {
              return <Task key={key}
                           text={this.state.tasks[key].text}
                           remove={() => this.removeTask(key)}/>
            }): <p>Add new task</p>}
        </div>
        <button onClick={this.goBack} className='Back'>Go back</button>
      </div>
    );

    this.state.loading ? toDoList = <Spinner/> : null;

    return (
      <div className="ToDoList">
        {toDoList}
      </div>
    );
  }
}

export default ToDoList;

import React from 'react';
import './Task.css';

const Task = (props) => {
    return (
        <div className='Task'>
            <p>{props.text}</p>
            <span onClick={props.remove}>x</span>
        </div>
    );
};

export default Task;
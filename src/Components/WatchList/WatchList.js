import React, {Component} from 'react';
import NewFilm from "../NewFilm/NewFilm";

class WatchList extends Component {

  shouldComponentUpdate(nextProps) {
    return nextProps.films !== this.props.films;
  }

  render () {
    return (
      <div className='WatchList'>
        <p>To watch list:</p>
        {this.props.films.map((film,index) => {
          if (this.props.films.length > 0) {
            return (
              <NewFilm name={film.name}
                       key={film.id}
                       remove={() => this.props.remove(film.id)}
                       change={(event) => this.props.change(event, index)}/>
            );
          }
        })}
      </div>
    );
  }
}

export default WatchList;
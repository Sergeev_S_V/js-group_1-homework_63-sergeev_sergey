import React from 'react';
import Task from "../Task/Task";

// const Tasks = props => {
//     return (
//         <div className='tasks'>
//             {props.tasks.length > 0
//               ? props.tasks.map(task => <Task text={task.text}
//                                       key={task.id}
//                                       remove={() => props.removeTask(task.id)}
//                                       done={task.checkbox}
//                                       checkbox={() => props.checkBox(task.id)}/>)
//               : null}
//         </div>
//     );
// };

const Tasks = props => {
    console.log(props.tasks)
  return (
    <div className='tasks'>
      {Object.keys(props.tasks).map(key => {
        // return console.log(props.tasks[key]);
        // console.log(props.tasks[key].)
        return <Task text={props.tasks[key].text}
             key={key}
             remove={() => props.removeTask(props.tasks[key].id)}
             done={props.tasks[key].checkbox}
             checkbox={() => props.checkBox(props.tasks[key].id)}/>
      })}
    </div>
  );
};
// return <Task text={task.text}
//              key={task.id}
//              remove={() => props.removeTask(task.id)}
//              done={task.checkbox}
//              checkbox={() => props.checkBox(task.id)}/>

export default Tasks;
import React, {Component} from 'react';
import './AddForm.css';

class AddForm extends Component {

  shouldComponentUpdate(nextProps) {
    return nextProps.name !== this.props.name ||
            nextProps.click !== this.props.click;
  }

  render () {
    return (
      <div className="AddForm">
        <h3>Welcome to <i>"Watch films"</i> app</h3>
        <button onClick={this.props.back} className='Back'>Go back</button>
        <input className='EnterText'
               onChange={this.props.name}
               type="text" placeholder='enter a film'/>
        <button className='Add' onClick={this.props.click}>ADD</button>
      </div>
    );
  }
}

export default AddForm;


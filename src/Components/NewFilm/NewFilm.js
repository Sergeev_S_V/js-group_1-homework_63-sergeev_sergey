import React, {Component} from 'react';
import './NewFilm.css';

class NewFilm extends Component {

  shouldComponentUpdate(nextProps) {
    return nextProps.name !== this.props.name;
  }

  render () {
      return (
        <div className='NewFilm-Form'>
          <input className='NewFilm-editFilm'
                 value={this.props.name}
                 type="text"
                 onChange={this.props.change}
                 onBlur={this.props.blur}/>
          <button className='NewFilm-remove'
                  onClick={this.props.remove}>X</button>
        </div>
      );
  }
}


export default NewFilm;
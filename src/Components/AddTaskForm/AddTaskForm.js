import React from 'react';
import './AddTaskForm.css';

const AddTaskForm = (props) => {
    return (
        <div className='AddTaskForm'>
            <input className='AddTask' type="text" onChange={props.text}/>
            <button className='AddBtn' onClick={props.add}>Add task</button>
        </div>
    );
};

export default AddTaskForm;
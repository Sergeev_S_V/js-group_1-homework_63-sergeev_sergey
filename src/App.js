import React, { Component } from 'react';
import {Route, Switch} from "react-router-dom";

import SwitchApp from "./Containers/SwitchApp/SwitchApp";
import WatchFilms from "./Containers/WatchFilms/WatchFilms";
import ToDoList from "./Containers/ToDoList/ToDoList";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Switch>
          <Route path='/' exact component={SwitchApp}/>
          <Route path='/todolist' exact component={ToDoList}/>
          <Route path='/watchfilms' exact component={WatchFilms}/>
          <Route render={() => <h1>404 Not found</h1>}/>
        </Switch>
      </div>
    );
  }
}

export default App;
